## NOTE: R package version 1.nnn corresponds to git tag 'vnnn'. I.e. drop the '1.' and prepend a 'v'

# 1.95
* fixed mito plot: it used to display mito_reads/non_mito_reads per cell, now shows
   mito_reads/all_reads (all_reads is any but ERCCs)

# 1.94
* now using the new SCutils::find.wellsets() to simplify matters
* goodwells.plot():
  * degraded mode has been much simplified: if no good ERCC's can be found, only
    the top3 are used. There is no 'half degraded' mode anymore
  * now shows log2(mean(goodERCCs)) and its density for all wells   
    including any ignored
  * goodwells.plot() shows no more details about full and empty because that is irrelevant
* stats output:
  the find.wellsets() introduces a number of additional wellsets (see its docu), 
  they are now also all dumped to the stats file.
* default species is now human

# 1.93
* changes to do with properly managing ignored wells, which are now
  implicit in whatever data columns there are.  The 'all' wellset has
  been replaced with 'allknown' and 'wanted'. The latter is the
  setdifference 'allknown - ignored'. plate.plot now plots whatever is in
  the data matrix, the rest is left white. 
* the stats output now also contains the members of all the relevant
  wellsets
* platediagnostics:::.gitversion() now works

# 1.92  (21-Jul-2021)

* cumulative inset in complexity plot replaced with simple inverse
  cumulative distr in main plot (oce library wouldn't compile and is enormous
  overkill anyway)

# 1.91  (22-Jun-2021)

* mito.plot plots % mitochondrial reads vs log2(txpts), not vs log2(ERCCs)

# 1.90  (10-May-2021)

* support for yeast

# 1.89  ( 7-May-2021)

* reporting gitversion using new system

# 1.88  (11-Mar-2021)

* script and docu installed in proper place

# 1.87  (25-Jan-2021)

* minor fixes discovered while backporting changes to platediagnostics2

# 1.86  (18-Jan-2021)

* Now be installed using devtools::install_bitbucket("princessmaximacenter/platediagnostics")

# 1.85  (15-Jan-2021)

* split off from Sharq/platediagnostics for for
  easier installing and maintenance. Now has its own repository.

* uuutils not needed anymore
