File `post-commit` is hook script that updates the `GIT_VERSION` file for the
`.gitversion()` function. Install it as follows, _before `R CMD INSTALL`-ing
the R package itself_ :

    cd PACKAGE_TOPLEVEL
    cp -f git_hooks/post-commit .git/hooks/post-commit # DONT SYMLINK
    chmod a+x .git/hooks/post-commit
    .git/hooks/post-commit  # run it once so we have the current commit!
    cd .git/hooks
    for f in checkout merge rebase; do
      ln -fs post-commit post-$f
    done
    cd -
   
The above code installs the hook under three different names so that it
runs upon all the relevant git actions.  Note that hooks can change by
unknowningly after e.g. a `git pull`. Since hooks run automatically this
can therefore be dangerous, so instead of symlinking, double check the
content and copy it as indicated above.

This script assumes a Linux system. If you're on a Mac, install the
Homebrew coreutils and make sure that `basename`, `dirname`, `readlink`
are the Linux versions, if needed by creating symlinks roughly as:

    cd /usr/local/bin # or: cd /opt/homebrew/bin for newer Macs
    ln -s gbasename basename  # and likewise for dirname and readlink

and by making sure that `/usr/local/bin` (or `/opt/homebrew/bin` for newer Macs)
come first in your `$PATH`
