# Important

The file(s) in this directory should be copied (not linked) under their
own name to $my_working_copy/.git/hooks/, roughly as follows:

```
$ cp -f post-commit    ../.git/hooks/post-commit  # this updates inst/GIT_VERSION
$ cp -f post-commit    ../.git/hooks/post-checkout  # this updates inst/GIT_VERSION

```

# Very important:
These scripts are run blindly, which constitutes an ideal attack vector
for someone with write-access to your repository. Therefore, never ever
symlink or hardlink the current file into the .git/hooks
directory. Instead, inspect it, then copy it manually to .git/hooks.

