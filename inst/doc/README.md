<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [The platediagnostics report](#the-platediagnostics-report)
- [General remarks](#general-remarks)
- [Mapping and feature types](#mapping-and-feature-types)
- [Well types](#well-types)
- [Glossary](#glossary)
- [The tables and plots](#the-tables-and-plots)
  - [Execution and Pre-mapping statistics](#execution-and-pre-mapping-statistics)
    - [Execution info](#execution-info)
    - [Pre-mapping statistics](#pre-mapping-statistics)
  - [Post-mapping statistics](#post-mapping-statistics)
    - [Rows](#rows)
    - [Columns](#columns)
    - [Remainder](#remainder)
    - [note regarding the *.platediagnostics.stats.txt file](#note-regarding-the-platediagnosticsstatstxt-file)
  - [ERCC txpts per well (`goodwells`).](#ercc-txpts-per-well-goodwells)
  - [transcripts distribution (`distribution`)](#transcripts-distribution-distribution)
  - [cumulative transcripts (`cumulative`)](#cumulative-transcripts-cumulative)
  - [percentage mitochondrial (`mito`)](#percentage-mitochondrial-mito)
  - [txpts vs ERCCs (`txptsVerccs`)](#txpts-vs-erccs-txptsverccs)
  - [transcripts in empty wells (`emptywells`)](#transcripts-in-empty-wells-emptywells)
  - [genes per txpt (`genesVtxpts`)](#genes-per-txpt-genesvtxpts)
  - [Plate plots](#plate-plots)
    - [% mapped raw reads (`rawpercplate`)](#%25-mapped-raw-reads-rawpercplate)
    - [mapped transcripts (`mappedtxptsplate`)](#mapped-transcripts-mappedtxptsplate)
    - [ERCC txpts (`ERCCplate`)](#ercc-txpts-erccplate)
  - [Accumulation plots](#accumulation-plots)
    - [gene accumulation per well (`accugenes`)](#gene-accumulation-per-well-accugenes)
    - [UMI accumulation per well (`accuumis`)](#umi-accumulation-per-well-accuumis)
- [Complexity](#complexity)
  - [Top plots](#top-plots)
    - [top expressed genes (`topexpr`)](#top-expressed-genes-topexpr)
    - [top varying genes (`topvar`)](#top-varying-genes-topvar)
  - [Contamination](#contamination)
    - [contamination within plate (`withinContam`)](#contamination-within-plate-withincontam)
    - [contamination from outside (`outsideContam`)](#contamination-from-outside-outsidecontam)
  - [oversequencing (`overseq`)](#oversequencing-overseq)
  - [Warnings (`warnings`)](#warnings-warnings)
  - [Session Information (`session`)](#session-information-session)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# The platediagnostics report

This document describes the content and interpretation of the
platediagnostics report that is produced by the `platediagnostics.R`
script. For notes and installing and running the script, see the
`README` in top-level of this R library.

# General remarks

 * For a good understanding of the report, it is important to know the
   essential steps of the pipeline. For this, 
   [see our BioArXiv paper](https://doi.org/10.1101/250811) or the
   brief description under 'The stages of the pipeline' in the top-level
   `README` of this R library.
 * For brevity of reference, the plots are named by a short mnemonic
   name, visible in orange at in the margin. 
 * The ordering of panels in the report is meant to be 
   logical. A slight deviation is that the four most important plots
   (pre- and post-mapping statistics, and `goodwells` and
   `distribution`) are in the top-left corner, so that they can be
   zoomed easily with all remaining visible.
 * Be aware of the difference between 'empty' (left empty for control purposes)
   and 'ignored' (wells to be discarded based on sorting or sample
   considerations).
 * Plots and numbers aimed at showing the technical quality of the run
   refer to _all_ wells, i.e. including those that were specified as 'ignore'.
   Conversely, plots and numbers relating to biological material
   always _exclude_ wells that were specified as 'ignore'.
 * Unless stated otherwise, numbers refer to transcript counts (not to
   UMI counts, nor to counts of raw reads).
 * Unless stated otherwise, 'transcript' means 
   'a unique mRNA molecule' that is mapped and 
   likely to be biologically meaningful, namely any of the GenCode
   features as well as the protein-coding gene clusters (see below).
   The following reads/transcripts are excluded: those mapping to ERCCs
   or mitochondrial genes (they are too variable), and those that map
   not uniquely, or map in the antisense direction. Those mapping to the
   the class of well-known antisense transcripts as annotated by GenCode
   (see below) are however included when speaking of 'transcript'.
 * This documentation may not always be up-to-date as we keep changing
   things in the report. Please let us know factual errors,
   inconsistencies, inaccuracies etc!

# Mapping and feature types

We currently use the following feature types and mapping types.
Most come from, or are derived directly from the GenCode annotations.

 * protein-coding: (hopefully) the majority of read mappings
 * protein-coding clusters: a feature that is added by us to 'rescue'
   reads that would otherwise assign ambiguously for genes that
   have overlapping exons. Reads mapping to the non-overlapping exons
   are assigned to the respective protein-coding feature; reads mapping
   to the overlapping exons are assigned to the cluster feature. Cluster
   features are mapped to the member gene that has most annotations (or has
   a lower EnsEMBL protein id)
 * lincRNA+AS: Long Intergenic non-coding RNAs and *known* antisense
 * other: all other GTF features. This includes things like tRNA, rRNA,
   miRNA and processed pseudo genes.
 * mito: mitochondrial genes
 * ERCC: external spike-ins
 * antisense: protein coding in antisense direction. This category is different
   from the lincRNA+AS category, as this refers to reads mapping in
   antisense direction to protein-coding genes that are *not known* to
   have antisense expression. The aim of this feature type is to check
   that the mapper is not too eager to map reads to 
   locations that are probably wrong
 * genomic: reads that map, but for which annotation is currently known
   (we currently do not distinguish between 'genomic' and 'intronic';
   a separate 'intronic' feature type be added in the near future)
 * non-unique: reads mapping to several locations in the genome
 * unmapped: reads that can't be placed

We use this variety of feature types to better know where all the reads
go, and to increase the likelihood of finding novel biologically
important results.

# Well types
 
 * UNK or 'unknown': reads for which the well barcode was not readable
   (even allowing for some mismatches). In many cases, the read itself
   can nonetheless be mapped. Such reads are kept for reference, as
   their numbers can e.g. used as those of an independent 'average
   well'. Note that for the unknown category we do not have UMI counts
   (and therefore also no transcript counts). The reason is that UMIs
   are only valid in combination with the well and gene a read maps to
 * empty well: a well into which all reagents and ERCCs were
   deposited, but deliberately no cellular material. Which set of wells 
   is left empty varies per plate.
 * non-empty well (also 'full'): a well into which all reagents including
   ERCCs were deposited, as well as cellular material of (hopefully!)
   one cell. In our plates there are 368 non-empty wells.
 * failed well: a well where the reactions did not work (based on ERCC
   counts).  Opposite of good well. 
 * good well: a well where the reactions did work (based on ERCC
   counts).  Opposite of failed well.
 * live well: a 'non-empty' and 'good' well that is deemed to contain
   material from a live cell, based on transcripts counts and the automatically
   determined liveness cutoff. 
 * dead well: a 'non-empty' and 'good' well that is deemed to *not*
   contain material from a live cell, based on transcript counts

Wells that are **ignored** (as specified using the `--ignore
*specification*` command line argument) are discarded prior to any other
use of any wells.  They are not considered when determining cutoffs for good
reactions or liveness. (The only exception is the 'over all' (but not 'per
well') accumulation plots, which make use of the overal numbers recorded
in the *.saturation.txt input file).

Note that the empty/non-empty label is orthogonal to the good/failed
label: all 4 combinations occur.

# Glossary
 * CBC: cell bar code (which is a slight misnomer as this barcode is used  to indentify wells, not cells)
 * feature: a well-known 'thing' with coordinates in the genome (e.g. protein-coding gene)
 * ERCC: (from 'External RNA Controls Consortium'): a read that derives
   from externally-spiked mRNA controls. Used for establishing if the
   reactions in a well worked or not
 * spike-in: see ERCC
 * transcript, txpt: a unique mRNA molecule stemming from one transcriptional
   event in one cell, and which is likely to be biologically meaningful.
   Transcript counts are based on the binomially corrected UMI
   counts. This is the best estimate of the original number of mRNA
   molecules in the starting material. If all UMIs are used up 
   (in our setup with a UMI-length of 6 nucleotides
   that is 4096 UMIs), the
   theoretical best estimate becomes infinite. 
   In such (exceedingly rare!) cases
   we therefore pretend we found 4095.5 UMIs, yielding a maximum of
   36908.7 transcripts.
   Do not confuse transcripts (numbers) with 'transcript isoforms' (which is what 
   Ensembl calls a transcript), nor with 'reads' (which seems to be more
   common when speaking about bulk RNA sequencing).
 * raw reads: reads present in the `fastq` file (from the sequencer) and
   in the mapped output from `STAR`. Due to amplification biases they
   are not a good measure of mRNA abundance in the starting material
 * UMI counts, umis: counts based on Unique Molecular Identifiers.  They
   are largely immune to amplification biases. They are still not the
   best estimate of abundance, since at high numbers, the UMI repertoire
   gets exhausted. For this, a binomial correction must be done to get
   the transcript counts, see transcript.
 * LF1 ratio: ratio of items (genes, umis) seen in the Last 1% of all reads and
    same in first 1% of all reads (shown in the accumulation plots)
 * LMFM ratio: ratio of items (genes, umis) seen in the Last Million of all reads and
    same in First Million of all reads (shown in the accumulation plots)

# The tables and plots

## Execution and Pre-mapping statistics

### Execution info
Most fields speak for themselves. Version refers to the version of the
script: git branch, followed by most recent tag, followed by the number
of commits since then, the abbreviated SHA1. *If the version string ends
in 'dirty', there have been (unknown) changes since that commit!*

### Pre-mapping statistics

All numbers are raw read counts:

 * total: number of reads in the `fastq` file that came off the
   sequencer (equal to the number of `.fastq` lines divided
   by 4). The number includes, of course, reads that will fail to map.
 * trimmed: the number of reads still longer than 20 nt after trimming
   (statistics on how much was trimming was done on which percentage of
   reads may be added. This can also be obtained from FastQC). 
 * usable: number of raw reads that are uniquely sense-mapping, and
   that are not mapping to ERCC or mitochondrial genes (OK, so this is
   post-mapping :-). The 'usable' reads are those expected to be
   biologically meaningful. This includes the reads for which
   the CBC could not be determined due to sequencing error. In the 
   `*.platediagnostics.stats.txt` table, this is the sum of rows 'prot', 'clus'
   'as+linc' and 'other' in column 'all'.
 * rescued CBCs: number of reads whose cell bar code had mismatches, but
   could still be identified by allowing 1 mismatch. Should be less
   than ~8%. Note that mismatches are only allowed for uppercase letters in the 
   barcode file. Lower-case letters indicate that a mismatch is not allowed
   in that position.
 * Invalid UMIs: UMIs containing 'N'. Should be less than 0.2%
 * Exhausted UMIs: number of times that a single
   well-gene combination used up *all* the available 4096
   UMIs. Very rare. Indicative of one or more of: more than one cell per
   well; very highly active genes; overamplification

## Post-mapping statistics

Numbers and percentages refer to alignments, not reads (even though for
brevity the term 'read' is used below, sorry!) The 'grand total' is the
number listed in the topleft of the table (row `total`, column
`total`). The grand total is *more* than that listed under the
`pre-mapping statistics`, because the former is the number of reads, but
the grand total is the number of *mappings* (i.e., alignment lines in
the `.bam` file), which is not unique for multimappers.  The number of
non-unique mappings is in row `non-unique`.  In column `total`, the row
`total` is the sum of the rows under it.

(There is a minute counting error: for an unknown reason, the grand
total reported in this table is on average ~ 0.02 % lower than the
number of alignment lines in the `.bam` file.  We will look into this at
some point).

Rows are feature type/mapping types and columns are well types, both
described above.

 * green font: numbers of wells
 * black font absolute numbers
 * blue font: percentages
 * red font: number of raw reads per well

### Rows
 * `#wells`-row: the number of wells per well type. The sum of  `live` +
    `dead` well numbers is always equal to the `nonempty,good react`
    well number.
 * `total`-row: total reads per well type. The columns `dead` and `live`
    to the right of the black line
    contain reads in wells from the `nonempty,good` type and are therefore
    not part of the sum shown in the `total`-column.
    The total/total number is all the alignments, including those
    for which the well barcode is unknown.
 * `%`-row, percentages of total reads per well type.
    Again, only numbers left of the black vertical line add up to 100%
    as these columns represent reads in mutually exclusive sets of wells
 *  Further rows: number per feature type or mapping type

### Columns

 * `total` column: total reads, including reads with unknown well barcode.
 * `%`-column, below horizontal black line: percentages of reads
  (alignments) per feature type, of *known* wells only, i.e. excluding
  the virtual 'unknown' well. The cells below the black line sum to 100%.
 * `unk. well`-column: percentage reads without barcode,
   again summing to 100% below the below black line.

### Remainder

 * The red numbers in the table are numbers of reads with given feature
   type and well type

### note regarding the *.platediagnostics.stats.txt file

The 'raw numbers' used to create the above table are provided in file
`*.platediagnostics.stats.txt`. The `all` column is the sum of the
`fullgood`, `fullfailed`,`emptygood` and `emptyfailed` columns,
corresponding to all known wells excluding the 'unknown well'.  This is
also identical to the sum of `full` and `empty`, and to the sum of
`good` and `failed`. The sum of all the `all` cells and all the `unk`
cells sums to the total sum of alignments in the topleft cell.
 
## ERCC txpts per well (`goodwells`).

The ERCC transcript counts are only used to establish in which well all
the reactions worked. The procedure is rather involved to achieve
robustness. It does a pre-selection of wells in which sufficiently
amplifying ERCCs species are identified (the so-called 'good ERCCs').
If there are not enough 'good ERCCs', the thresholds are lowered, and 
the plot will get a prominent 
*`running in degraded mode, few good ERCCs, bad plate`* notice. 
If still not enough ERCC's are found, the 3 most abundant ERCCs are used,
and the notice  becomes
*`running in degraded mode, no good ERCCs, awful plate`*.

The abundance and variance of the selected ERCCs are used to determine a
threshold which is subsequently applied to all wells.  Details can be
found in the source code and documentation of functions `goodERCCs` and
`goodwells` in package `SCutils`. The current criteria are
a bit involved so there is not a single cutoff that we can display in the
plot.

The plot shows the distribution of the mean ERCC counts (log2 scale) in
'good' (black) and 'failed' (red) wells. The individual counts are also
shown as little ticks on the x-axis. 

The purpose of the plot is to show that the criteria for calling a well
'failed' or 'good' worked correctly. The failed and and non-failed
distributions should be well separated.
All wells are taken into account (i.e. ignored
wells are also taken along).

The percentage given on top refers to the total number of wells (384).

Axes scales are fixed at  `0 < x < 10,  0 < x < 0.8`

The names of the top 10 'good ERCCs' that were used to determine the wells are
shown inside the plot, including their median and interquartile range
across all wells. 

## transcripts distribution (`distribution`)
<sub>note: the panel is below that of the post-mapping statistics</sub>

This plot depicts the distribution of biological transcript counts over
empty and non-empty wells (both having good reactions), as well as the
cutoff for liveness.  (Note that the terms 'live' and 'dead' refer
exclusively to good, non-empty wells).

The liveness of cells is based on the abundance of transcripts (uniquely
sense-mapping, and excluding ERCC and mitochondrial; see above). The
threshold for calling a cell/well 'live' is based on the abundance and
variance of these (biological!) transcripts in *empty* wells (that had
good reaction). I.e., we are using the 'contamination' in empty wells as
the 'noise level' that wells have to exceed.  The threshold is the
median of transcipts in good empty wells, + 3 times their MAD (function
`livecellcutoff` in package `SCutils`). Wells with counts below this are
called 'dead', those with counts above it 'live'. Note that this liveness
cutoff may not be appropriate for in-depth analyses. 

The counts in the empty wells are shown in purple (magnified 5 times for
visibility), those in non-empty wells in green.  The y-axis autoscales;
the x-axis is logarithmic with a fixed scale `2 < log2(x) < 16` (i.e. `4
< x < 65,536`), unless any well contains more than 200,000
transcripts. In that (exceptional!) case, the x-axis will autoscale and
a warning is displayed.

An important purpose of the plot is to show if the automatic liveness
cutoff was determined correctly. There should be a clear separation
between the empty and non-empty wells, with the liveness cutoff close to
the x-value where their densities cross. The liveness cutoff now (July
2020) has a minimum of 500. This cutoff is also used if there are too
few empty wells with good enough ERCCs.

The numbers shown refer to the reads mapped to any of the biological
features that are present in the count table: protein coding,
protein-coding clusters, known lincRNA+AS.  Mitochondrially mapping
reads are included; genomically mapping reads, ERCCs and antisense reads
are ignored.

## cumulative transcripts (`cumulative`)

This plot shows the number inverse cumulative distribution of the
previous distribution plot next to it, i.e. the number of cells having
more than a given number of transcripts.  Only biological transcripts in
non-empty wells having good reactions are shown. The liveness cutoff is
indicated by a red dash line.

## percentage mitochondrial (`mito`)

This plot shows the distribution of the fraction of mitochondrial
transcripts in the various well types.

X scale is fixed at `0 < log2(x) < 10 (i.e. 1 < x < 1,024)` 
Y scale is fixed `0% < y < 100%`, but autoscaling if it exceeds 100%.

High percentages *may* be indicative of stressed or degraded input
material (the reason is probably that nuclear cytosolic mRNA is likely
to degrade earlier than RNA that is still 'protected' by the
mitochondrial membranes). However, cells from e.g. muscle tissue have
naturally higher numbers of mitochondrial genes expressed, so take with
a grain of salt.

The distributions are generally similar for good and failed wells, and
also for empty and non-empty wells.

## txpts vs ERCCs (`txptsVerccs`)

This plot shows the behaviour of transcript counts in relation to
the ERCCs counts for the various well types.

X scale is fixed at `0 < log2(x) < 10` (i.e. `1 < x < 1,024`) 
Y scale is fixed `2 < log2(y) < 15` (i.e. `4 < y < 32,768`), but autoscaling if Y exceeds that.

The plot is meant as a different / combined view of the good/failed and
live/dead decisions shown in plots `goodwell` and `distribution`.
Failed and good cells should be well separated in the x-direction.
Dead and live cells/wells in should be separated in the y-direction
with the latter having y-coordinates similar to those of the empty wells.
The red dashed line represents the cutoff.

## transcripts in empty wells (`emptywells`)

This plot gives an overview of the biological (but not mitochnondrial)
transcipts and ERCCs in the empty wells. Bars are labeled by their well
name (plate coordinates). Those with 'X' below are failed empty wells.
The y-axis shows log2(counts+1) and is autoscaling.

The plot is meant to show more detail than is possible in the preceeding
plots.

## genes per txpt (`genesVtxpts`)

This plot shows the number of distinct genes expressed per well as a
function of the number of transcripts in that well. Axes are in
log-scale and are autoscaling.

Usually, the scatter plot is close to linear, but curving slightly
downward, implying that more highly expressed genes are rarer. The
median is typically around 1 - 3 transcripts per gene.  Wells deviating
below the main cloud have a limited repertoire of highly expressed
genes.

It could be argued that the axes should be flipped, but we *measure*
transcripts and *infer* the gene complexity, hence this orientation.

## Plate plots

The following three plots show the behaviour of quantities across the
physical plate. Wells having a downward pointing triangle printed over
the well were specified as 'ignore'.  Similarly, wells with a white dot
in the centre were specified as empty wells. This gives a visual check
of whether the correct empty and ignored (if any) wells were specified
by the user.

Wells marked by the algorithm with an **X** are wells where the
reactions failed, those marked with a **\+** are dead wells. Color
scales are indicated.

The purpose of these plots is to easily spot effects that are spatial in
nature. They may arise from problems with robot pin tips, differential
evaporation, etc., but also in case different cells or treatment types
were deliberately deposited in different locations.

### % mapped raw reads (`rawpercplate`)

The quantities shown refer to raw reads, because for the unmapped reads
we don't have UMI counts (UMIs are only valid in combination with the
well and gene a read maps to).

The color scale is fixed between 0% and 100%.

Empty wells should again show clearly.  In case the plate contains
different materials (cell types, treatments) in different regions, this
may be visible in this plot.

The numbers shown refer to the reads mapped to any of the biological
features that are present in the count table: protein coding,
protein-coding clusters, known lincRNA+AS.  Mitochondrially mapping
reads are included; genomically mapping reads, ERCCs and antisense reads
are ignored.

Empty wells are marked with a white dot,
failed wells are marked with an X, ignored wells are overplotted
with a triangle. Dead wells are marked with a +.

### mapped transcripts (`mappedtxptsplate`)

Pin problems are visible as pairs of rows with a bluer color (lower
counts). The empty wells should be much bluer than the rest of the
plate. In case the plate contains different materials (cell types,
treatments) in different regions, this may be visible in this plot.

The numbers shown refer to the reads mapped to any of the biological
features that are present in the count table: protein coding,
protein-coding clusters, known lincRNA+AS.  Mitochondrially mapping
reads are _excluded_, as are genomically mapping reads, ERCCs and
antisense reads. The color scale is autoscaling.

Empty wells are marked with a white dot,
failed wells are marked with an X, ignored wells are overplotted
with a triangle. Dead wells are marked with a +.

### ERCC txpts (`ERCCplate`)

This plot is the clearest for exposing spatial artefacts such as pin
defects. In the ideal case (and when none of the wells failed), this
plot should be of a completely uniform color.

The color scale is fixed `-1 < log2(x) < 14`, i.e.  `0.5 < x < 16,384`.

Only ERCC's are shown. Empty wells are marked with a white dot,
failed wells are marked with an X. Ignored wells and dead wells
are *not* marked.

## Accumulation plots

While sequentially processing the mappings in the BAM file produced by
the mapper, the `tally.pl` script logs, every 10,0000 raw reads, the
number of distinct genes, umis and transcripts it has encountered, to
the so-called saturation files (ERCCs, mitochondrial reads and unknown
well-reads are ignored).  The aim of this is to gauge the extra
information on genes, UMIs and transcripts that can be gained when
sequencing more deeply. (This works because the raw reads in our BAM
files are not sorted, so looking at just the first 10 million reads is
like having sequenced to a depth of only 10M; processing the next 10
million reads is like sequencing more deeply by an extra 10 million
reads). If additional sequencing library material is still available,
the curves may be helpful to decide whether additional sequencing of
that material is likely to be useful.

The plot shows plain counts per well (black curves) in absolute and
relative scales (left).  The blue curves show the derivative (growth) of
the accumulation, expressed as the extra items per well gained in the
previous one million reads. This is also shown using a percentage scale,
relative to the number items seen in the first 1 Million reads.  Both
axes are autoscaling. All curves reflect numbers of properly mapping
reads in any well (including empty and failed), and exclude
mitochondria, ERCCs.

The accumulation curves strongly resemble Michaelis-Menten behaviour. We
therefore model them as such (dashed black line), allowing us to predict
the plateau value, as well as the sequencing depth required to reach 90%
of this plateau value. The fit summary is shown in center of the plot,
including the goodness of the fit and the number of items seen in the
first 1 million reads (so as to make it independent of the sequencing
depth). The fits are done for averages over all wells (not just live) as this is
more likely to be representative of how a future run might turn out.

Additionally, the fit is done for just the first 1 million reads, and
lastly two ratios are reported: the LF1 and LMFM ratios. LF1 is the
number of new items in the Last 1% of all reads, relative to number of
same in the First 1% of all reads. LMFM is similar but reports the
number of new items in the Last 1 Million of all reads, relative to the
same in the First 1 Million. The 'end' of the blue curve is exactly the
LMFM ratio.

If there are any wells being ignored (see above), the 'over all'
accumulation plots include a warning that these plots (and fits etc.)
are based on all wells, i.e. not discarding the ignored wells. This is
because it is based on the input data in *.saturation.txt which does
not know about ignored wells.

### gene accumulation per well (`accugenes`)

Shows the accumulation of the mean number distinct genes per well. 

### UMI accumulation per well (`accuumis`)

This plot shows the accumulation of the mean number distinct UMIs per
well. They should serve as a proxy for transcript counts which we don't
record.

# Complexity

This plot shows the distribution of the complexity per cell, i.e. the
numbers of unique genes per live well. Axes are autoscaling.  The
inverse distribution is shown in red.  It represents the fraction of
cells having expression of more genes than the x-axis value.

The plot gives a general impression of the overal richness of transcript
species per well. This is expected to be similar for similar input
material. Generally, the larger, the better.  However, very specialized
cell types can be expected to have low complexity.

## Top plots

These plot give an overview of the (at most) 50 most abundantly
expressed genes and most varying genes. The axes are autoscaling.

These plots are there for general QC purposes, e.g. to check the
presence or absence genes that are (un)expected to be abundant or highly
varying.

### top expressed genes (`topexpr`)

Shows the most abundantly expressed genes. Pay attention to expected and
unexpected genes.

### top varying genes (`topvar`)

As the `topexpr` plot, but for the most varying genes, expressed as the
so-called index of dispersion, i.e., the variance/mean. 

## Contamination

These plots show the presence of 'extraneous' material in wells. Minute
amounts of contamination are inevitable, and in fact we make thankful
use of this when establishing a threshold for liveness of cells (see
above). These plots are mean to document the extent of the
contamination. Only the 50 most abundant genes in empty wells, and 200
most abundant genes in non-empty wells are considered (in both cases
only 'good' wells).

### contamination within plate (`withinContam`)

Highly abundant transcripts of genes in the non-empty wells are more
likely to end up as 'contamination' in the empty wells, and as such
could be called 'within plate contamination'. This barplot shows
this. The percentage given inside the bars is the fraction of
transcripts in empty wells relative to those found in the non-empty
wells. At most 20 most abundant genes found in both empty and non-empty
wells are shown.

### contamination from outside (`outsideContam`)

Transcripts that are highly abundant in the empty wells but not so much
in the non-empty wells must have come from outside the current
plate. This plot shows at most 20 of the 50 genes that are most abundant
in the empty wells, but which are *not* among the in the top 200 most
abundant genes in non-empty wells.

## oversequencing (`overseq`)

This plot shows the distribution of the ratios of raw reads per UMI per
gene, in live wells only. The median shown on top is that of the
unlogged ratio. Note that the horizontal scale is logarithmic.

If the raw reads per UMI ratio is, on average, much larger than one,
much of the sequencing effort may be wasted. Compare also with the
accumulation plots. Note however that the UMIs represented by just one
read is always highest, so 'wasting' may be too strong a term.

## Warnings (`warnings`)

This text shows the first 24 lines of `R` warnings (if any).  The
complete set of warnings can be found in *outputname*.warnings.txt
produced as a by-product of the `platediagnostics.R` script.

## Session Information (`session`)

The output from `sessionInfo()`, showing various details on the R session.
Again, only the first 24 lines are shown, the full output can be found
in *outputname*.warnings.txt.
