#!/bin/sh
# -*- mode: sh; -*-

## create some very simplistic test data
## the data in this current directory was created with it based on plate TM87

testdata=~/git/Sharq/platediagnostics/test/data

cd some/where

for i in  *sat*txt; do 
   head -100 $i > $testdata/$i 
done

mito='__MT-' # note the dash! E.g. MTCO3P20 is called 'mitochondrially encoded' but lies on chr22 !?
for i in   *.rawcounts.* *.umicounts.*  *.transcripts.txt; do 
  (head -500 $i | grep -v $mito
   grep $mito $i | head -20
   tail -500 $i | grep -v $mito ) > $testdata/$i
done
 
