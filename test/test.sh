#!/bin/bash
# -*- mode: sh; -*-

# Simple test script for testing the platediagnostics code
# in the current directory.  Simply execute it in the current test
# directory where it will analyze the data in `testdata` and produce
# output in testout (which is ignored by git).
# 
# If a directory is provided as an argument, the data in that directory
# is analyzed. In that case, a file *.plateDiagFiles.txt should be
# present to be used as the --filesFrom argument to platediagnostics.
# (the '--empties' argument may need attention ...)
#
# See also $GIT/sc-facility/miscUtilities/platediagnostics-standalone.sh
#

if [ $# -eq 1 ]; then
    datadir=$1
else
    datadir=testdata
fi

set -x

root=$(realpath $(dirname $BASH_SOURCE))/
outputdir=$(realpath .)/testout

rdir=$(realpath $root/..)

rm -fr $outputdir 2>/dev/null
mkdir -p $outputdir
stdout=$outputdir/stdout
stderr=$outputdir/stderr
touch $stdout $stderr
stdout=`realpath $outputdir/stdout`
stderr=`realpath $outputdir/stderr`

tmpRlib=`realpath $outputdir`

R CMD INSTALL --library=$tmpRlib $rdir
rc=$? 
if [ $rc  -ne 0  ]; then
    echo "Could compile platediagnostics, exiting" >&2
    exit $rc
fi

cd $datadir

env R_LIBS=$tmpRlib:$R_LIBS $rdir/inst/scripts/platediagnostics.R \
   --outputDir $outputdir \
   --plate testout \
   --pdf testout.pdf \
   --empties col24odd \
   --accumulation_percentage 90 \
   --ignore A4,B5,C6,D7,E8,F9,G10,H11,I12,K13,L14,M15,N16 \
   --filesFrom test.plateDiagFiles.txt \
   --debug --verbose > $stdout 2>$stderr

rc=$? 
if [ $rc  -ne 0  ]; then
    echo "Exit status was $rc. See $stdout and/or $stderr" >&2
    exit $rc
fi

warnings=`realpath $outputdir/testout.warnings.txt`

## Put strings signifying serious errors (which may contain spaces but no
## wildcards) between the ZZZZZZZZZ. Any of them is sufficient to make
## the script exit with an error:
while IFS='' read msg; do 
   if grep -iq "$msg" $warnings $stdout $stderr; then
       echo "Found serious errors ('$msg'), see $warnings" >&2
       exit 1
   fi
done<<ZZZZZZZZ
error
not a multiple
non-  
closure
ZZZZZZZZ

path=`realpath $outputdir`
echo -e "All OK\nAll output is in\n$path\n(can be removed)"
## rm -fr ../testout

exit 0
