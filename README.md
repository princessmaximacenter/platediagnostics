<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Platediagnostics](#markdown-header-platediagnostics)
- [Installation](#markdown-header-installation)
    - [Prerequisites](#markdown-header-prerequisites)
- [Running the script](#markdown-header-running-the-script)
    - [The stages of the pipeline](#markdown-header-the-stages-of-the-pipeline)
- [Required input files](#markdown-header-required-input-files)
    - [Log files from the various steps:](#markdown-header-log-files-from-the-various-steps)
    - [Files output by `tally.pl`:](#markdown-header-files-output-by-tallypl)
        - [Main output](#markdown-header-main-output)
- [Exit status and output files](#markdown-header-exit-status-and-output-files)
- [Documentation](#markdown-header-documentation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Platediagnostics

`platediagnostics` is an R library (and driver script) for quality
control of a scRNA-Seq run on a 384 wells plate using the CEL-Seq2
protocol and the scripts found in the
https://bitbucket.org/princessmaximacenter/scseq repository.

This is a much diverged fork of https://github.com/MauroJM/single-cell-sequencing by
Mauro Muraro (van Oudenaarden group, Hubrecht Institute, Utrecht).

The main script is platediagnostics.R, which is intended to run as an
`Rscript` script (Unix only, for now), and run either stand-alone or
under the control of the Cromwell WDL execution machine.

# Installation

## Prerequisites

Current prerequisites are the libraries `SCutils` (see https://bitbucket.org/princessmaximacenter/scutils/) 
and whatever there is in `DESCRIPTION` file. 

The easiest way to install the packages is using 

## Installing

1. If not yet installed, install `devtools` as:

```
  install.packages("devtools")
```

2. Install the `SCutils` and `platediagnostics` packages as:

```
devtools::install_bitbucket("princessmaximacenter/scutils")
devtools::install_bitbucket("princessmaximacenter/platediagnostics")
```

# Running the script

Do `platediagnostics.R --help` to see the options.

The `platediagnostics.R` script is currently easiest to use in
conjunction with the Cromwell execution environment.  There are quite a
few input files needed (such as log files from earliers stages of the
pipeline), and Cromwell collects their names into a file that can be
used as the argument to the `--filesFrom` parameter. Alternatively, use
`--prefix` to specify a filename pattern that evaluates to a list of
input files. 

Lastly, you can run the script interactively (e.g. inside Rstudio), and
specify arguments inside the `if(interactive()){ ...}` clause.

## The stages of the pipeline

To understand what each input file means, here is a brief description of
the stages currently used

 * prepocessing: combine read1 (containing UMI and well barcode) with 
   read2 (the mRNA of interest). Also concatenate all the lanes into one
   file (script `preprocess_fastq.pl`)
 * trimming: apply homopolynucleotide,
   sequencing quality and complexity triming (the `*trim*` scripts)
 * mapping: place the trimmed reads onto the genome, yielding just
   coordinates (`STAR.sh` script)
 * addFeatures: given the coordinates, assign read counts to genomic
   features such as genes,long intergenic non-coding RNA, etc. (script
   `add_features.pl`)
 * tallying: sort reads by well and by gene, calculate abundances, statistics
   and accumulation curves (script `tally.pl`)

# Required input files 

The following input files are needed; they should be listed in the file
given to the --filesFrom`-parameter, or `--prefix=`*name*` should
evaluate to them.

The files are listed 'chronologically'.

## Log files from the various steps:

Most of these are only there for troubleshooting and for passing 
information to `platediagnostics.R` (in the form of a JSON object).

 * *name*`.preProcessFastq.log` - produced by `preprocess_fastq.pl`
 * *name*`.compTrim.log` - produced by `multiCompTrim.py`
 * *name*`.STARlog.final.out` - STAR's `Log.final.out` file
 * *name*`.STAR.log` - produced by `STAR.sh`
 * *name*`.addFeatures.log`  - produced by `add_features.pl`
 * *name*`.processSAM.log` - produced by `tally.pl`

## Files output by `tally.pl`:
### Main output
 * *name*`.rawcounts.txt` - raw read counts
 * *name*`.umicounts.txt` - counts based on UMIs
 * *name*`.transcripts.txt` - as umicounts, but corrected for saturation
### Antisense output (currently not used)
 * *name*`.as_rawcounts.txt` - 
 * *name*`.as_umicounts.txt` - 
 * *name*`.as_transcripts.txt` - 
### Accumulation curves:
 * *name*`.saturation.txt` - overal accumulation per gene, umi and transcript
 * *name*`.wellsat.genes.txt` - accumulation per gene per well 
 * *name*`.wellsat.umis.txt` - accumulation per umi per well 

# Exit status and output files

The exit status is 0 for succes, 1 otherwise. *There is no `stdout` nor
`stderr`*.

The output files (when run non-interactively) are:

  * *name*.pdf - The QC report (default is `platediagnostics.pdf`)
  * *name*.warnings.txt - Combined `stdout` and `stderr`
  * *name*.stats.txt - Absolute transcript counts by
      feature type and well category, used in the 'post-mapping
    statistics' table
  * *name*.live.txt - The transcript count data for just the live cells
   and excluding ERCCs, mitochondrial genes, and genes with a zero
   transcript count of zero in any well. Wether a well contained a live cell
   is determined by an automatic liveness cutoff (see the QC report), which is
   generally not appropriate for in-depth analyses.

# Documentation

Extensive documentation on the contents and interpretation of the resulting report
can be found in the `doc` subdirectory.

